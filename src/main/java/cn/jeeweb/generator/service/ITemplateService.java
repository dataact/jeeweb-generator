package cn.jeeweb.generator.service;

import cn.jeeweb.common.mybatis.mvc.service.ICommonService;
import cn.jeeweb.common.mybatis.mvc.service.ICommonService;
import cn.jeeweb.generator.entity.Template;

/**   
 * @Title: 生成模板
 * @Description: 生成模板
 * @author jeeweb
 * @date 2017-09-15 15:10:12
 * @version V1.0   
 *
 */
public interface ITemplateService extends ICommonService<Template> {

}

